$("#get-time").click(function getTime() {
    let now = new Date();
    let dayOfWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"][(new Date()).getDay()];
    let nowHour = now.getHours();
    let nowMin = now.getMinutes();
    let nowSec = now.getSeconds();
    if (nowHour < 12) {
        $("#time").html("Current time : " + nowHour + "AM :" + nowMin + ":" + nowSec);
    }
    else {
        $("#time").html("Current time : " + (nowHour - 12) + "PM :" + nowMin + ":" + nowSec);
    }
    $("#dayOfWeek").html("Today is: " + dayOfWeek);
    setTimeout(getTime, 1000);
})
