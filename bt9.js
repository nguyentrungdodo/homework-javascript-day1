$("#get-Ex9").click(function () {
    let valueEx9 = $("#valueEx9").val();
    let data = valueEx9.split(',').map(function (item) {
        return parseInt(item, 10);
    })
    let overlap = 0;
    let count = 1;
    let item;
    for (let i = 0; i < data.length; i++) {
        overlap = 0;
        for (let j = i; j < data.length; j++) {
            if (data[i] == data[j]) overlap++;
            if (overlap > count) {
                count = overlap;
                item = data[i];
            }
        }
    }
    $("#check-Ex9").html(item);

})