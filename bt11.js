$("#get-Ex11").click(function () {
    let valueEx11 = $("#valueEx11").val();
    let data = valueEx11.split(',').map(function (item) {
        return parseInt(item, 10);
    })
    let count = 0;
    let result = [

    ];
    data.sort();
    for (let i = 0; i < data.length; i++) {
        for (let j = i + 1; j < data.length; j++) {
            if (data[j] % data[i] == 0) {
                count++;
                result.push([data[i], data[j]]);
            }
        }
    }
    let text = "";
    for (let i = 0; i < result.length; i++) {
        text += "[" + result[i] + "], ";
    }
    $("#check-Ex11").html(count + " - " + text);

})