$("#get-Ex15").click(function () {
    let valueEx15 = $("#valueEx15").val();
    let data = valueEx15.split(' ').map(function (item) {
        return item;
    })
    let max = data[0].length;
    let count = 0;
    for (let i = 1; i < data.length; i++) {
        if (max <= data[i].length) {
            max = data[i].length;
            count = i;
        }
    }
    $("#check-Ex15").html("&#8216;" + data[count] + "&#8216;");
})