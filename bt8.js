$("#get-Ex8").click(function () {
    let firstValueEx8 = $("#firstValueEx8").val();
    let secondValueEx8 = $("#secondValueEx8").val();
    let data1 = firstValueEx8.split(',').map(function (item) {
        return parseFloat(item);
    })
    let data2 = secondValueEx8.split(',').map(function (item) {
        return parseFloat(item);
    })
    let kt = 0;
    let kt1 = [];
    let kt2 = []
    if (data1.length != data2.length) $("#check-Ex8").html("false");
    else {
        for (let i = 0; i < data1.length; i++) {
            if (data1[i] != data2[i]) {
                kt++;
                kt1.push(data1[i]);
                kt2.push(data2[i]);
            }
            if (kt > 2) break;

        }
        if (kt == 0) $("#check-Ex8").html("false");
        else {
            if (kt != 2) $("#check-Ex8").html("true");
            else {
                if ((kt1[0] == kt2[1]) && (kt1[1] == kt2[0])) $("#check-Ex8").html("true");
                else $("#check-Ex8").html("true");
            }
        }

    }
})